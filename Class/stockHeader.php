<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of stockHeader
 *
 * @author HP
 */
class stockHeader {
    public function stockHeader1($stockHeader) {
        $data['nameComplement'] = $stockHeader->nameComplement;
        $data['id'] = $stockHeader->id;
        $data['code'] = $stockHeader->code ;
        $name =  explode('|',$stockHeader->name);
        if (isset($name[0])) $data[]['name1'] = $name[0];
        if (isset($name[1])) $data[]['color'] = $name[1];
        if (isset($name[2])) $data[]['size'] = $name[2];
        $data['sellingPrice'] = $stockHeader->sellingPrice;
        $data['count'] = $stockHeader->count;
        return $data;
    }
}
