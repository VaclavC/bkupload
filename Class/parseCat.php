﻿<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of parseCat
 *
 * @author HP
 */
class parseCat {
    public function categories($instSlug){
        
$xml1 = simplexml_load_file('kategorieIntObch_01_v2.0.xml' );

foreach ($xml1->xpath('//rsp:responsePackItem') as $x1){

        // načtení namespaců
        $namespaces = $x1->getNameSpaces(true);
        
        $lst = $x1->children( $namespaces['lst'] );
        
        if (is_array($lst->listCategory->categoryDetail) || ($lst->listCategory->categoryDetail) instanceof Traversable ){
        foreach ($lst->listCategory->categoryDetail as $listCategory){
        
        $ctg = $listCategory->children ($namespaces['ctg']);
            foreach( $ctg->category as $category) {
                $this->rekurs($category, $space = 0,$instSlug);
                } 
            }
        }   
    }
} 
    
private function rekurs($value, $space, $instSlug, $id=NULL, $id1=NULL){
        if (is_array($value)|| ($value) instanceof Traversable) {
                $value = json_decode(json_encode($value), TRUE);
                        foreach ($value as $key=>$intParameterValue1) {
                            if ($space == 0) $id = $value['id'];
                            if (isset ($value['id']) && $value['id']!=0 && $space===1) $id1=$value['id'];
                            if ($key=='name' and !is_array($intParameterValue1)){
                                //echo($id.'|'.$id1.'|'."&nbsp;".$value['id']."&nbsp;".$key." : ".$intParameterValue1.'<br>');
                                $own_id = $value['id'];
                                    if ($id1 == $value['id']) 
                                            $parent_id = $id;
                                                else 
                                                     $parent_id = $id1;
                            
                            $category = array('id'=>$own_id, 'parent_id'=>$parent_id, 'title'=>$intParameterValue1,'slug'=>$instSlug->getSlug($intParameterValue1));
                            if (Db::queryOne('SELECT `id` FROM `category` WHERE `id` = ? ', array($own_id))){
                                Db::zmen('category', $category, 'WHERE `id` = ?', array($own_id));
                            }
                                else
                                    Db::vloz('category', $category);
                                
                                $space = $space+1;
                            }
                            if (is_array($intParameterValue1)) $this->rekurs($intParameterValue1, $space, $instSlug, $id, $id1);
                        }
                return;
            }
        }
}
